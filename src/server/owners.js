module.exports = {
    owners: getOwners()
};

function getOwners() {
    return [{
        'name': 'Eric',
        'draft': {
            'TOP_LANE': ['CLG', 'H2K'],
            'JUNGLER': ['C9', 'VIT'],
            'MID_LANE': ['TL', 'FOX'],
            'AD_CARRY': ['TSM', 'FOX'],
            'SUPPORT': ['TSM', 'VIT']
        },
        'transactions' : [
            {'week' : 2, 'pos' : 'JUNGLER', 'out' : 'VIT', 'in' : 'G2'},
            {'week' : 2, 'pos' : 'MID_LANE', 'out' : 'FOX', 'in' : 'G2'},
            {'week' : 2, 'pos' : 'AD_CARRY', 'out' : 'FOX', 'in' : 'IMT'},
            {'week' : 5, 'pos' : 'SUPPORT', 'out' : 'VIT', 'in' : 'TL'},
            {'week' : 6, 'pos' : 'MID_LANE', 'out' : 'TL', 'in' : 'C9'},
            {'week' : 6, 'pos' : 'SUPPORT', 'out' : 'TL', 'in' : 'VIT'},
            {'week' : 6, 'pos' : 'AD_CARRY', 'out' : 'TSM', 'in' : 'C9'},
            {'week' : 6, 'pos' : 'SUPPORT', 'out' : 'TSM', 'in' : 'C9'}
        ]
    }, {
        'name': 'Matt',
        'draft': {
            'TOP_LANE': ['NRG', 'OG'],
            'JUNGLER': ['NRG', 'TSM'],
            'MID_LANE': ['IMT', 'NRG'],
            'AD_CARRY': ['NRG', 'REN'],
            'SUPPORT': ['FOX', 'TL']
        },
        'transactions' : [
            {'week' : 4, 'pos' : 'SUPPORT', 'out' : 'TL', 'in' : 'UOL'}
        ]
    }, {
        'name': 'Mickey',
        'draft': {
            'TOP_LANE': ['TL', 'VIT'],
            'JUNGLER': ['REN', 'TL'],
            'MID_LANE': ['H2K', 'OG'],
            'AD_CARRY': ['C9', 'H2K'],
            'SUPPORT': ['NRG', 'OG']
        },
        'transactions' : [
            {'week' : 2, 'pos' : 'JUNGLER', 'out' : 'REN', 'in' : 'UOL'},
            {'week' : 4, 'pos' : 'AD_CARRY', 'out' : 'C9', 'in' : 'G2'},
            {'week' : 4, 'pos' : 'TOP_LANE', 'out' : 'TL', 'in' : 'G2'}
        ]
    }, {
        'name': 'Pojo',
        'draft': {
            'TOP_LANE': ['TSM', 'EL'],
            'JUNGLER': ['CLG', 'H2K'],
            'MID_LANE': ['TSM', 'DIG'],
            'AD_CARRY': ['CLG', 'TL'],
            'SUPPORT': ['ROC', 'H2K']
        },
        'transactions' : [
        ]
    }, {
        'name': 'Trevor',
        'draft': {
            'TOP_LANE': ['IMT', 'UOL'],
            'JUNGLER': ['OG', 'IMT'],
            'MID_LANE': ['C9', 'CLG'],
            'AD_CARRY': ['OG', 'VIT'],
            'SUPPORT': ['IMT', 'CLG']
        },
        'transactions' : [
            {'week' : 3, 'pos' : 'AD_CARRY', 'out' : 'VIT', 'in' : 'UOL'},
            {'week' : 4, 'pos' : 'SUPPORT', 'out' : 'CLG', 'in' : 'G2'},
            {'week' : 4, 'pos' : 'MID_LANE', 'out' : 'C9', 'in' : 'UOL'},
            {'week' : 7, 'pos' : 'AD_CARRY', 'out' : 'OG', 'in' : 'VIT'}
        ]
    }, {
        'name': 'Tyler',
        'draft': {
            'TOP_LANE': ['C9', 'FNC'],
            'JUNGLER': ['FOX', 'FNC'],
            'MID_LANE': ['FNC', 'VIT'],
            'AD_CARRY': ['DIG', 'FNC'],
            'SUPPORT': ['C9', 'FNC']
        },
        'transactions' : [
            {'week' : 6, 'pos' : 'AD_CARRY', 'out' : 'DIG', 'in' : 'TSM'},
            {'week' : 6, 'pos' : 'SUPPORT', 'out' : 'C9', 'in' : 'TSM'}
        ]
    }
    ];
}
