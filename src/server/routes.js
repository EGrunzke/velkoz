var router = require('express').Router();
var four0four = require('./utils/404')();
var rData = require('./riot.js');
var fData = require('./owners.js');

router.get('/riot', getRiot);
router.get('/live', getLive);
router.get('/owners', getOwners);
router.get('/*', four0four.notFoundMiddleware);

module.exports = router;

//////////////

function getRiot(req, res, next) {
    res.status(200).send(rData.riotData);
}

function getOwners(req, res, next) {
    res.status(200).send(fData.owners);
}

function getLive(req, resa, next) {
    var http = require('http');
    http.get({
        host: 'fantasy.na.lolesports.com',
        path: '/en-US/api/season/12'
    }, function(res) {
        // explicitly treat incoming data as utf8 (avoids issues with multi-byte chars)
        res.setEncoding('utf8');

        // incrementally capture the incoming response body
        var body = '';
        res.on('data', function(d) {
            body += d;
        });

        // do whatever we want with the response once it's done
        res.on('end', function() {
            try {
                var parsed = JSON.parse(body);
                resa.status(200).send(parsed);
            } catch (err) {
                console.log('Parse failed for getLiveData');
            }
        });
    }).on('error', function(err) {
        // handle errors with the request itself
        console.log('XHR Failed for getLiveData');
    });
}
