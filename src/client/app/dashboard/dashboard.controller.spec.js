/* jshint -W117, -W030 */
describe('DashboardController', function() {
    var controller;
    var owners = mockData.getMockOwners();
    var week = mockData.getMockWeek();

    beforeEach(function() {
        bard.appModule('app.dashboard');
        bard.inject('$controller', '$log', '$q', '$rootScope', 'dataservice');
    });

    beforeEach(function () {
        sinon.stub(dataservice, 'getOwners').returns($q.when(owners));
        sinon.stub(dataservice, 'getLocalData').returns($q.when(week));
        sinon.stub(dataservice, 'getLiveData').returns($q.when(week));
        controller = $controller('DashboardController');
        $rootScope.$apply();
    });

    bard.verifyNoOutstandingHttpRequests();

    describe('Dashboard controller', function() {
        it('should be created successfully', function () {
            //expect(controller.rawStats).to.be.defined;
        });

        describe('after activate', function() {
            it('should have title of Dashboard', function () {
                expect(controller.title).to.equal('Dashboard');
            });

            it('should have logged "Activated"', function() {
                expect($log.info.logs).to.match(/Activated/);
            });

            it('should have player data for week 1', function() {
                expect(controller.stats['0'].length).to.equal(105);
            });
        });
    });
});
