// jscs:disable maximumLineLength
(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$q', 'dataservice', 'logger'];
    /* @ngInject */
    function DashboardController($q, dataservice, logger) {
        var vm = this;
        vm.pKeys = [
            'name',
            'proTeamId',
            'position',
            'pMatchesPlayed',
            'matchesPlayed',
            'pKills',
            'kills',
            'pDeaths',
            'deaths',
            'pAssists',
            'assists',
            'pMinionKills',
            'minionKills',
            'pDoubleKills',
            'doubleKills',
            'pTripleKills',
            'tripleKills',
            'pQuadraKills',
            'quadraKills',
            'pPentaKills',
            'pentaKills',
            'pKillOrAssistBonus',
            'killOrAssistBonus'
        ];
        vm.tKeys = [
            'id',
            'name',
            'shortName',
            'pMatchesPlayed',
            'matchesPlayed',
            'pFirstBlood',
            'firstBlood',
            'pTowerKills',
            'towerKills',
            'pBaronKills',
            'baronKills',
            'pDragonKills',
            'dragonKills',
            'pMatchVictory',
            'matchVictory',
            'pMatchDefeat',
            'matchDefeat',
            'pSecondsPlayed',
            'secondsPlayed',
            'pQuickWinBonus',
            'quickWinBonus'
        ];
        //vm.posKeys = [ TOP_LANE, JUNGLER, MID_LANE, AD_CARRY ];
        vm.multipliers = {
            TOP_LANE: {kills: 286.8694814, deaths: -98.81201477, assists: 99.12654852, minionKills: 1.920640492},
            JUNGLER: {kills: 423.2848589, deaths: -106.2711016, assists: 101.4918219, minionKills: 2.768335342},
            MID_LANE: {kills: 207.9179416, deaths: -145.6207967, assists: 129.4348354, minionKills: 2.3325633},
            AD_CARRY: {kills: 198.8611763, deaths: -192.0560599, assists: 125.8723615, minionKills: 2.565267552},
            SUPPORT: {kills: 642.0835668, deaths: -40.63398825, assists: 90.82637103, minionKills: 8.251079929},
            TEAM: {dragonKills: 52.09276018, baronKills: 143.90625, towerKills: 17.11895911}
        };
        vm.positionTeamMultiplier = {
            TOP_LANE: 5 / 6,
            JUNGLER: 5 / 6,
            MID_LANE: 1,
            AD_CARRY: 1,
            SUPPORT: 2 / 3
        };
        vm.positionShortNames = {
            TOP_LANE: 'Top',
            JUNGLER: 'Jun',
            MID_LANE: 'Mid',
            AD_CARRY: 'ADC',
            SUPPORT: 'Sup'
        };
        vm.news = {
            title: 'velkoz!',
            description: 'Hot Towel Angular is a SPA template for Angular developers.'
        };
        vm.messageCount = 0;
        vm.rawStats = {};
        vm.rawOwners = {};
        vm.stats = {};
        vm.teams = {};
        vm.teamMap = {};
        vm.owners = {};
        vm.p2o = {};
        vm.currentWeek = '1';
        vm.title = 'Dashboard';

        activate();

        function activate() {
            var promises = [getMessageCount(), getStats(), getOwners()];
            return $q.all(promises).then(function () {
                var weeks = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
                parseStats(weeks);
                parseOwners(weeks);
                calcStandings();
                createP2O();
                logger.info('Activated Dashboard View');
            });
        }

        function getMessageCount() {
            return dataservice.getMessageCount().then(function (data) {
                vm.messageCount = data;
                return vm.messageCount;
            });
        }

        function getOwners() {
            return dataservice.getOwners().then(function (data) {
                vm.rawOwners = data;
                return vm.rawOwners;
            });
        }

        function getStats() {
            return dataservice.getLiveData().then(function (data) {
                vm.rawStats = data;
            });
        }

        function parseOwners(weeks) {
            weeks.forEach(function (weekNum) {
                vm.owners[weekNum] = vm.rawOwners.map(function (o) {
                    var owner = {
                        name: o.name,
                        players: []
                    };

                    var ps = [];
                    Object.keys(o.draft).forEach(function (dPos) {
                        o.draft[dPos].forEach(function (dTeam) {
                            ps.push({p: dPos, t: dTeam});
                        });
                    });

                    o.transactions.filter(function (tx) {
                        return tx.week <= weekNum;
                    }).forEach(function (tx) {
                        ps.forEach(function (pt) {
                            if (pt.p === tx.pos && pt.t === tx.out) {
                                pt.t = tx.in;
                            }
                        });
                    });

                    ps.forEach(function (pt) {
                        vm.stats[weekNum].filter(function (player) {
                            return player.position === pt.p && player.proTeamId.shortName === pt.t;
                        }).forEach(function (player) {
                            owner.players.push(player);
                        });
                    });

                    owner['points'] = owner.players.map(function (p) {
                        return p.totalPoints;
                    }).reduce(function (a, b) {
                        return a + b;
                    }, 0);
                    return owner;
                });
            });
        }

        function createP2O() {
            vm.rawOwners.forEach(function (o) {
                Object.keys(o.draft).forEach(function (dPos) {
                    o.draft[dPos].forEach(function (dTeam) {
                        vm.p2o[dPos + dTeam] = o.name;
                    });
                });

                o.transactions.forEach(function (tx) {
                    delete vm.p2o[tx.pos + tx.out];
                    vm.p2o[tx.pos + tx.in] = o.name;
                });
            });
        }

        function parseStats(weeks) {
            weeks.map(function (w) {
                return parseStatWeek(w);
            });
        }

        function parseStatWeek(weekNum) {
            vm.teams[weekNum] = vm.rawStats.proTeams.map(function (t) {
                var tVals = [t.id, t.name, t.shortName].concat(t.statsByWeek[weekNum]);
                var tObj = zip({}, vm.tKeys, tVals);
                tObj.position = 'TEAM';
                calcPoints(tObj, tObj.secondsPlayed);
                tObj.totalPoints *= (1 + tObj.quickWinBonus * 0.15);
                tObj.totalPoints += tObj.matchVictory * 3;
                return tObj;
            });

            vm.teams[weekNum].forEach(function (t) {
                vm.teamMap[t.id] = t;
            });

            vm.stats[weekNum] = vm.rawStats.proPlayers.map(function (p) {
                var pVals = [p.name, vm.teamMap[p.proTeamId], p.positions[0]].concat(p.statsByWeek[weekNum]);
                var player = zip({}, vm.pKeys, pVals);
                player['shortPosition'] = vm.positionShortNames[player.position];
                return player;
            }).filter(function (p) {
                return p.matchesPlayed > 0;
            }).map(function (p) {
                var m = p.proTeamId.secondsPlayed / 60;
                var totalTeamPoints = p.proTeamId.totalPoints;
                p['teamScore'] = totalTeamPoints;
                var keys = Object.keys(vm.multipliers[p.position]);
                var total = 0;
                keys.forEach(function (k) {
                    var spm = p[k] / m;
                    var mul = vm.multipliers[p.position][k];
                    var pts = spm * mul;
                    p[k + 'points'] = pts;
                    total += pts;
                });
                p['playerPoints'] = total;

                var matchesPlayedMultiplier = (p.matchesPlayed / 2);
                p['teamPoints'] = totalTeamPoints * vm.positionTeamMultiplier[p.position] * matchesPlayedMultiplier;

                p['totalPoints'] = total + p['teamPoints'];
                return p;
            });
        }

        function calcStandings() {
            calcStandingsImpl(vm.stats);
            calcStandingsImpl(vm.teams);
            calcStandingsImpl(vm.owners);
        }

        function calcStandingsImpl(parent) {
            var itemMap = {};
            var totals = [];
            parent['0'] = totals;

            Object.keys(parent).forEach(function (week) {
                parent[week].forEach(function (item) {
                    itemMap[item.name] = combine(itemMap[item.name], item);
                });
            });

            Object.keys(itemMap).forEach(function (key) {
                totals.push(itemMap[key]);
            });
        }

        function combine(o1, o2) {
            if (o1 === undefined) {
                return o2;
            }

            var o3 = {};
            Object.keys(o1).forEach(function (key) {
                if (key === 'players') {
                    o3[key] = combinePlayers(o1[key], o2[key]);
                }
                else {
                    var sum = o1[key] + o2[key];
                    o3[key] = isNaN(sum) ? o1[key] : sum;
                }
            });
            return o3;
        }

        function combinePlayers(masterArray, weekArray) {
            var combinedArray = [];
            var names = {};
            // Keep track of which players from the master get added to
            masterArray.forEach(function (p) {
                names[p.name] = true;
            });

            weekArray.forEach(function (weekPlayer) {
                var match = masterArray.filter(function (totalPlayer) {
                    return weekPlayer.name === totalPlayer.name;
                });
                if (match.length === 0) {
                    combinedArray.push(weekPlayer);
                }
                else {
                    combinedArray.push(combine(match.pop(), weekPlayer));
                    names[weekPlayer.name] = false;
                }
            });

            // Any players from the master array that didn't acquire new stats still need to be added
            masterArray.forEach(function (p) {
                if (names[p.name]) {
                    combinedArray.push(p);
                }
            });
            return combinedArray;
        }

        function zip(obj, keys, values) {
            for (var i = 0; i < keys.length; i++) {
                obj[keys[i]] = values[i];
            }
            return obj;
        }

        function calcPoints(entity, secondsPlayed) {
            var minutes = secondsPlayed / 60;
            entity['minutes'] = minutes;
            var keys = Object.keys(vm.multipliers[entity.position]);
            var total = 0;
            keys.forEach(function (key) {
                var spm = entity[key] / minutes;
                var mul = vm.multipliers[entity.position][key];
                var pts = spm * mul;
                entity[key + 'Points'] = pts;
                total += pts;
            });
            entity['totalPoints'] = total;
            return entity;
        }
    }
})();
