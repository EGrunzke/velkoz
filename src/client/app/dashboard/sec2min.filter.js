(function () {
    'use strict';

    angular
        .module('app.filters', [])
        .filter('sec2min', sec2min);

    function sec2min() {
        return sec2minImpl;

        ////////////////

        function sec2minImpl(sec) {
            var remainder = sec % 60;
            var remStr = remainder < 10 ? '0' + remainder.toString() : remainder.toString();
            var minStr = Math.floor(sec / 60).toString();
            return minStr + ':' + remStr;
        }
    }

})();

