/* jshint -W117, -W030 */
describe('Filter: sec2min', function() {
    var s2m;

    beforeEach(module('app.filters'));
    beforeEach(inject(function(_sec2minFilter_) {
        s2m = _sec2minFilter_;
    }));

    describe('sec2min', function() {
        it('should convert 60s to 1:00', function() {
            expect(s2m(10)).to.equal('0:10');
            expect(s2m(60)).to.equal('1:00');
            expect(s2m(61)).to.equal('1:01');
            expect(s2m(70)).to.equal('1:10');
            expect(s2m(80)).to.equal('1:20');
            expect(s2m(680)).to.equal('11:20');
            expect(s2m(719)).to.equal('11:59');
        });
    });
});
